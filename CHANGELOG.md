# XDG Base Directory Library Changelog
This document's purpose is to lay out and explain what exactly in the library
changes over time, new features that are added, and deprecations/removals.

### Versioning
An attempt at [Semantic Versioning](https://semver.org/) is used for the library
version number, where the version number is `XXX.YYY.ZZZ`. `XXX` coresponds to
the major number, and increments of that value are not backwards compatible.
`YYY` coresponds to the minor number, where features have been added in a
backwards compatible manner  (code utilizing this library should not have to
change **at all**). `ZZZ` is the patch number purely for bug fixes that are
completely backwards compatible.

#### Types of changes:
Changelog format type is inspired by [keep a changelog](https://keepachangelog.com).
 - `Added` for new features
 - `Changed` for changes in existing functionality
 - `Deprecated` for soon-to-be removed features
 - `Removed` for now removed features
 - `Fixed` for any bug fixes
 - `Security` for vulnerabilities
 - `Bug/Regression` for known bugs/regressions


## Incomplete/To Do
 - TODO :P

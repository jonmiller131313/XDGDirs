/// @file xdg_dirs.c

#include "jonmiller131313/xdg_dirs.h"

#define STR(x) #x

/// Cache variable storing the home directory.
static char *HOME_DIR = NULL;
/// Cache variable storing the name of the executing file.
static char *EXEC_FILE = NULL;

/// Cache variable storing the value of `$XDG_DATA_HOME`.
static char *XDG_DATA_HOME = NULL;
/// Cache variable storing the value of `$XDG_CONFIG_HOME`.
static char *XDG_CONFIG_HOME = NULL;
/// Cache variable storing the value of `$XDG_CACHE_HOME`.
static char *XDG_CACHE_HOME = NULL;
/// Cache variable storing the value of `$XDG_RUNTIME_DIR`.
static char *XDG_RUNTIME_DIR = NULL;

/// Cache variable storing the value of `$XDG_DATA_DIRS`, the last element is
/// `NULL`.
static char **XDG_DATA_DIRS = NULL;
/// Cache variable storing the value of `$XDG_CONFIG_DIRS`, the last element is
/// `NULL`.
static char **XDG_CONFIG_DIRS = NULL;


/**
 * @brief Appends a string to the end of another string while resizing it.
 *
 * @param dest   The string to store the result.
 * @param append THe string to append.
 *
 * @return `dest + append` where `dest` has been resized.
 *
 * @warning This function assumes that `dest` is dynamically allocated.
 */
char* append_resize_str(char *dest, const char *append) {
    return strcat(realloc(dest, strlen(dest) + strlen(append) + 1), append);
}

/**
 * @brief Reads a value from an environment variable and if not set or not a
 *        valid path then uses a default path relative to the `$HOME` variable.
 *
 * @param envVar     A pointer with the name of the env var to check, will
 *                   allocate space for the string to copy.
 * @param defaultVal The default path to use, it will be append to `$HOME/`.
 *
 * @return The resulting value.
 */
char* get_env_var_default(char **envVar, const char *defaultVal) {
    if(*envVar)
        return *envVar;

    *envVar = getenv(STR(*envVar));
    if(*envVar && *envVar[0] == '/') {
        *envVar = strdup(*envVar);
        return *envVar;
    }

    *envVar = append_resize_str(strdup(HOME_DIR), defaultVal);
    return *envVar;
}

/**
 * @brief Splits a list of absolute paths separated by `':'`. The resulting list
 *        is `NULL` terminated.
 *
 * @param pathList The string of paths.
 *
 * @return A dynamically allocated list of strings that is `NULL` terminated
 *         containing all of the absolute paths.
 */
char** split_path_list(const char *pathList) {
    char **splitList = NULL;
    if(!pathList)
        return NULL;

    char *popped = strchr(pathList, ':');
    if(!popped) {
        if(pathList[0] != '/') {
            return NULL;
        } else {
            splitList = calloc(2, sizeof(char*));
            splitList[0] = strdup(pathList);
            return splitList;
        }
    }

    size_t count = 2; // 1 extra for NULL terminator pointer.
    for(const char *c = pathList; *c; c++)
        count++;

    splitList = calloc(count, sizeof(char*));

    const char *old = pathList;
    size_t length = 0;
    size_t i = 0;
    size_t shrink = 0;
    do {
        if(*old != '/') {
            shrink++;
            popped++;
            old = popped;
            continue;
        }
        length = popped - old;

        splitList[i++] = strndup(old, length);

        popped++;
        old = popped;
    } while((popped = strchr(old, ':')) != NULL);
    if(*old == '/')
        splitList[i] = strndup(old, pathList + strlen(pathList) - old);
    else
        shrink++;

    if(shrink > 0)
        splitList = reallocarray(splitList, count - shrink, sizeof(char*));

    return splitList;
}

/**
 * @brief Searches for a file relative to a list of paths and returns the first
 *        result.
 *
 * @param pathList The list of paths.
 * @param file     The file to search for.
 *
 * @return The path that contains the file relative to it, or `NULL` if not
 *         found.
 */
const char* search_path_list(const char *const *pathList, const char *file) {
    char tmpPath[PATH_MAX] = {0};

    for(const char *const *path = pathList; *path; path++) {
        size_t len = strlen(*path);
        strcpy(tmpPath, *path);
        tmpPath[len] = '/';
        tmpPath[len + 1] = '\0';
        strcat(tmpPath, file);

        if(access(tmpPath, F_OK) != -1)
            return *path;
    }

    return NULL;
}

/// Reads `/proc/self/cmdline` to set `EXEC_FILE`.
void xdg_dirs_set_exec_file() {
    FILE *cmdlineFile = fopen("/proc/self/cmdline", "r");

    if(!cmdlineFile) {
        fprintf(stderr, "!!! Unable to open /proc/self/cmdline file!\n");
        EXEC_FILE = strdup("!!!ERROR!!!");
        return;
    }

    char *buffer = NULL;
    size_t zero = 0;
    ssize_t readSize = getdelim(&buffer, &zero, '\0', cmdlineFile);
    fclose(cmdlineFile);

    if(readSize == -1) {
        fprintf(stderr, "!!! Unable to read /proc/self/cmdline file!\n");
        free(buffer);
        EXEC_FILE = strdup("!!!ERROR!!!");
        return;
    }

    EXEC_FILE = strndup(buffer, readSize - 1);
    free(buffer);
}

/// Reads `$HOME` to set `HOME_DIR`.
void xdg_dirs_set_home_file() {
    HOME_DIR = getenv("HOME");

    if(!HOME_DIR) {
        char *startHome = "/home/";
        char *username = getenv("USER");
        if(!username) {
            startHome = "/tmp/";
            username = "xdg_dirs_home";
        }

        fprintf(stderr, "%s: (XDG_Dirs) Unable to read $HOME, using '%s'.\n",
                        EXEC_FILE,
                        append_resize_str(strdup(startHome), username));
        return;
    }

    HOME_DIR = strdup(HOME_DIR);
}

/// Initializes `EXEC_FILE` and `HOME_DIR`.
__attribute__((constructor(101)))
void xdg_dirs_init() {
    xdg_dirs_set_exec_file();
    xdg_dirs_set_home_file();
}

/// Frees up allocated memory.
__attribute__((destructor(101)))
void xdg_dirs_clean_up() {
    xdg_dirs_reset();
    free(HOME_DIR);
    free(EXEC_FILE);
}

void xdg_dirs_reset() {
    if(HOME_DIR) {
        free(HOME_DIR);
        xdg_dirs_set_home_file();
    }
    if(EXEC_FILE) {
        free(EXEC_FILE);
        xdg_dirs_set_exec_file();
    }

    if(XDG_CONFIG_HOME) {
        free(XDG_CONFIG_HOME);
        XDG_CONFIG_HOME = NULL;
    }
    if(XDG_CACHE_HOME) {
        free(XDG_CACHE_HOME);
        XDG_CACHE_HOME = NULL;
    }
    if(XDG_DATA_HOME) {
        free(XDG_DATA_HOME);
        XDG_DATA_HOME = NULL;
    }
    if(XDG_RUNTIME_DIR) {
        free(XDG_RUNTIME_DIR);
        XDG_RUNTIME_DIR = NULL;
    }

    if(XDG_DATA_DIRS) {
        for(char **i = XDG_DATA_DIRS; *i; i++)
            free(*i);
        free(XDG_DATA_DIRS);
        XDG_DATA_DIRS = NULL;
    }
    if(XDG_CONFIG_DIRS) {
        for(char **i = XDG_CONFIG_DIRS; *i; i++)
            free(*i);
        free(XDG_CONFIG_DIRS);
        XDG_CONFIG_DIRS = NULL;
    }
}


const char* xdg_dirs_config_home() {
    return get_env_var_default(&XDG_CONFIG_HOME, "/.config");
}

const char* xdg_dirs_cache_home() {
    return get_env_var_default(&XDG_CACHE_HOME, "/.cache");
}

const char* xdg_dirs_data_home() {
    return get_env_var_default(&XDG_DATA_HOME, "/.local/share");
}

const char* xdg_dirs_runtime_dir() {
    if(XDG_RUNTIME_DIR)
        return XDG_RUNTIME_DIR;

    XDG_RUNTIME_DIR = getenv("XDG_RUNTIME_DIR");
    if(!XDG_RUNTIME_DIR || XDG_RUNTIME_DIR[0] != '/')
        XDG_RUNTIME_DIR = NULL;

    XDG_RUNTIME_DIR = strdup(XDG_RUNTIME_DIR);
    return XDG_RUNTIME_DIR;
}

const char *const * xdg_dirs_data_dirs() {
    if(XDG_DATA_DIRS)
        return (const char *const *)XDG_DATA_DIRS;

    XDG_DATA_DIRS = split_path_list(getenv("XDG_DATA_DIRS"));
    if(!XDG_DATA_DIRS) {
        XDG_DATA_DIRS = calloc(3, sizeof(char*));
        XDG_DATA_DIRS[0] = strdup("/usr/local/share");
        XDG_DATA_DIRS[1] = strdup("/usr/share");
    }

    return (const char *const *)XDG_DATA_DIRS;
}

const char *const * xdg_dirs_config_dirs() {
    if(XDG_CONFIG_DIRS)
        return (const char *const *)XDG_CONFIG_DIRS;

    XDG_CONFIG_DIRS = split_path_list(getenv("XDG_CONFIG_DIRS"));
    if(!XDG_CONFIG_DIRS) {
        XDG_CONFIG_DIRS = calloc(2, sizeof(char*));
        XDG_CONFIG_DIRS[0] = strdup("/etc/xdg");
    }

    return (const char *const *)XDG_CONFIG_DIRS;
}

const char * xdg_dirs_search_data_dirs(const char* filename) {
    if(!XDG_DATA_DIRS)
        xdg_dirs_data_dirs();
    if(!XDG_DATA_HOME)
        xdg_dirs_data_home();

    char tmpPath[PATH_MAX] = {0};
    size_t len = strlen(XDG_DATA_HOME);
    strcpy(tmpPath, XDG_DATA_HOME);
    tmpPath[len] = '/';
    tmpPath[len + 1] = '\0';
    strcat(tmpPath, filename);
    if(access(tmpPath, F_OK) != -1)
        return XDG_DATA_HOME;

    return search_path_list((const char *const *)XDG_DATA_DIRS, filename);
}

const char * xdg_dirs_search_config_dirs(const char* filename) {
    if(!XDG_CONFIG_DIRS)
        xdg_dirs_config_dirs();
    if(XDG_CONFIG_HOME)
        xdg_dirs_config_home();

    char tmpPath[PATH_MAX] = {0};
    size_t len = strlen(XDG_CONFIG_HOME);
    strcpy(tmpPath, XDG_CONFIG_HOME);
    tmpPath[len] = '/';
    tmpPath[len + 1] = '\0';
    strcat(tmpPath, filename);
    if(access(tmpPath, F_OK) != -1)
        return XDG_CONFIG_HOME;

    return search_path_list((const char *const *)XDG_CONFIG_DIRS, filename);
}






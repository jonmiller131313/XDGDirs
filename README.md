# XDG Base Directory Library
[![Version](https://img.shields.io/badge/Version-1.0-informational.svg)](CHANGELOG.md)

The goal of this library is to implement the [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html) in a simple C library.

## Getting Started
### Dependencies:
 - C99
 - CMake 3.10
 - [Doxygen](https://doxygen.nl/) _(optional)_
 - [GoogleTest](https://github.com/google/googletest) _(optional)_

 ### Building/Installing
Clone the repo, then run:
```bash
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j
sudo make install
```

#### CMake options:
 - `-DENABLE_TESTS=YES`: Enables unit test with `check` make target
   - Requires GoogleTest
 - `-DDISABLE_DOCS=YES`: Disable generating documentation with `docs` target
   - Documentation generation is enabled by default, use this option to suppress missing Doxygen dependency warning.


## Including In Your Project
### CMake
If using the default CMake install location all you need to include in your CMakeLists.txt file is
```cmake
find_package(XDGDirs)
...
target_link_libraries(${TARGET} XDGDirs)
```
If it does not find the package, or you installed the library in a non-default location make sure to set the `-DCMAKE_PREFIX_PATH` variable or the `PATHS` option in `find_package()`.

### pkgconf
This project also includes a `XDGDirs.pc` file. By default on UNIX system it installs it to `/usr/local/lib/pkgconfig` so that path will need to be set in the `PKG_CONFIG_PATH` environmental variable before the `pkgconf` command is run so that the command can find the file.

**Usage:**
```bash
g++ source.cpp -c $(pkgconf --cflags XDGDirs) -o source.cpp.o
...
g++ source.cpp.o $(pkgconf --libs XDGDirs) -o a.out
```


cmake_minimum_required(VERSION 3.10)

include(GNUInstallDirs)
include(CMakePackageConfigHelpers)

# Configure CMake config file
set(LIBRARY_CONFIG "${LIBRARY_TARGET}Config")
configure_package_config_file(
    "${CMAKE_DIR}/${LIBRARY_CONFIG}.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY_CONFIG}.cmake"
    INSTALL_DESTINATION "${CMAKE_INSTALL_FULL_LIBDIR}/cmake/${LIBRARY_TARGET}"
)
# Write CMake version file
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY_CONFIG}Version.cmake"
    COMPATIBILITY SameMajorVersion
)

# Configure pkg-config file
set(LIBRARY_PKG_CONFIG "${LIBRARY_TARGET}.pc")
configure_file(
    "${CMAKE_DIR}/${LIBRARY_PKG_CONFIG}.in"
    "${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY_PKG_CONFIG}"
    @ONLY
)

# Install CMake package files
install(FILES
    "${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY_CONFIG}.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY_CONFIG}Version.cmake"
    DESTINATION "${CMAKE_INSTALL_FULL_LIBDIR}/cmake/${LIBRARY_TARGET}"
)

# Install pkg-config file
install(FILES
    "${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY_PKG_CONFIG}"
    DESTINATION "${CMAKE_INSTALL_FULL_LIBDIR}/pkgconfig"
)

# Install library
install(TARGETS ${LIBRARY_TARGET}
    DESTINATION ${CMAKE_INSTALL_FULL_LIBDIR}
)

# Install headers
install(DIRECTORY "${LIBRARY_INCLUDE_DIR}"
    DESTINATION ${CMAKE_INSTALL_FULL_INCLUDEDIR}
)

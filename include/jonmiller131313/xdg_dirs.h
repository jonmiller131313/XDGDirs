/**
 * @file xdg_dirs.h
 * @brief Simple [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html) library.
 * @copyright GPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of XDGDirs:
 *    https://gitlab.com/jonmiller131313/XDGDirs
 *
 *    XDGDirs is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    XDGDirs is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with wiringPi.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_XDG_DIRS_H
#define JONMILLER131313_XDG_DIRS_H

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/**
 * @brief Resets all of the cached environment variables. Next time one of the
 *        functions in this library is called it will reload the value.
 */
void xdg_dirs_reset();

/**
 * @brief Gets the directory where configuration files are stored.
 *
 * `$XDG_CONFIG_HOME` defines the base directory relative to which user specific
 * configuration files should be stored. If `$XDG_CONFIG_HOME` is either not set
 * or empty, a default equal to `$HOME/.config` should be used.
 *
 * @return The configuration files directory.
 */
const char* xdg_dirs_config_home();

/**
 * @brief Gets the directory where caches files are stored.
 *
 * `$XDG_CACHE_HOME` defines the base directory relative to which user specific
 * non-essential data files should be stored. If `$XDG_CACHE_HOME` is either not
 * set or empty, a default equal to `$HOME/.cache` should be used.
 *
 * @return The configuration files directory.
 */
const char* xdg_dirs_cache_home();

/**
 * @brief Gets the directory where data files are stored.
 *
 * `$XDG_DATA_HOME` defines the base directory relative to which user specific
 * data files should be stored. If `$XDG_DATA_HOME` is either not set or empty,
 * a default equal to `$HOME/.local/share` should be used.
 *
 * @return The configuration files directory.
 */
const char* xdg_dirs_data_home();

/**
 * @brief Gets the directory where runtime files are stored.
 *
 * `$XDG_RUNTIME_DIR` defines the base directory relative to which user-specific
 * non-essential runtime files and other file objects (such as sockets, named
 * pipes, ...) should be stored. The directory MUST be owned by the user, and he
 * MUST be the only one having read and write access to it. Its Unix access mode
 * MUST be `0700`.
 *
 * The lifetime of the directory MUST be bound to the user being logged in. It
 * MUST be created when the user first logs in and if the user fully logs out
 * the directory MUST be removed. If the user logs in more than once he should
 * get pointed to the same directory, and it is mandatory that the directory
 * continues to exist from his first login to his last logout on the system, and
 * not removed in between. Files in the directory MUST not survive reboot or a
 * full logout/login cycle.
 *
 * The directory MUST be on a local file system and not shared with any other
 * system. The directory MUST by fully-featured by the standards of the
 * operating system. More specifically, on Unix-like operating systems AF_UNIX
 * sockets, symbolic links, hard links, proper permissions, file locking, sparse
 * files, memory mapping, file change notifications, a reliable hard link count
 * must be supported, and no restrictions on the file name character set should
 * be imposed. Files in this directory MAY be subjected to periodic clean-up. To
 * ensure that your files are not removed, they should have their access time
 * timestamp modified at least once every 6 hours of monotonic time or the
 * 'sticky' bit should be set on the file.
 *
 * If `$XDG_RUNTIME_DIR` is not set applications should fall back to a
 * replacement directory with similar capabilities and print a warning message.
 * Applications should use this directory for communication and synchronization
 * purposes and should not place larger files in it, since it might reside in
 * runtime memory and cannot necessarily be swapped out to disk.
 *
 * @return The runtimetime directory.
 *
 * @todo Perform all of the checks required for this directory.
 */
const char* xdg_dirs_runtime_dir();

/**
 * @brief Gets the list of strings storing directory where data files can be
 *        found in.
 *
 * `$XDG_DATA_DIRS` defines the preference-ordered set of base directories to
 * search for data files in addition to the `$XDG_DATA_HOME` base directory. The
 * directories in $XDG_DATA_DIRS should be seperated with a colon `':'`.
 *
 * If `$XDG_DATA_DIRS` is either not set or empty, a value equal to
 * `/usr/local/share/:/usr/share/` should be used.
 *
 * @return A list of strings of directories, the last entry is `NULL`.
 */
const char *const * xdg_dirs_data_dirs();

/**
 * @brief Gets the list of strings storing directory where config files can be
 *        found in.
 *
 * `$XDG_CONFIG_DIRS` defines the preference-ordered set of base directories to
 * search for configuration files in addition to the `$XDG_CONFIG_HOME` base
 * directory. The directories in $XDG_CONFIG_DIRS should be seperated with a
 * colon `':'`.
 *
 * If `$XDG_CONFIG_DIRS` is either not set or empty, a value equal to `/etc/xdg`
 * should be used.
 *
 * @return A list of strings of directories, the last entry is `NULL`.
 */
const char *const * xdg_dirs_config_dirs();

/**
 * @brief Searches the data directories for a file.
 *
 * This searches `$XDG_DATA_HOME` first before searching `$XDG_DATA_DIRS`.
 *
 * @param filename The file to search for.
 *
 * @return The directory which has the file stored relative to it, or `NULL` if
 *         not found. If `data.txt` is passed into this function and it returns
 *         `/usr/share` then the file is stored in `/usr/share/data.txt`.
 */
const char* xdg_dirs_search_data_dirs(const char *filename);

/**
 * @brief Searches the config directories for a file.
 *
 * This searches `$XDG_CONFIG_HOME` first before searching `$XDG_CONFIG_DIRS`.
 *
 * @param filename The file to search for.
 *
 * @return The directory which has the file stored relative to it, or `NULL` if
 *         not found. If `data.txt` is passed into this function and it returns
 *         `/etc/xdg` then the file is stored in `/etc/xdg/data.txt`.
 */
const char* xdg_dirs_search_config_dirs(const char *filename);


#endif

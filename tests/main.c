#include "jonmiller131313/xdg_dirs.h"

#include <stdlib.h>

int main() {
    printf("XDG_DATA_HOME = '%s'\n", xdg_dirs_data_home());
    printf("XDG_CONFIG_HOME = '%s'\n", xdg_dirs_config_home());
    printf("XDG_CACHE_HOME = '%s'\n", xdg_dirs_cache_home());
    printf("XDG_RUNTIME_DIR = '%s'\n", xdg_dirs_runtime_dir());

    printf("\nXDG_DATA_DIRS:\n");
    const char *const *data_dirs = xdg_dirs_data_dirs();
    if(!*data_dirs) {
        printf("<empty>\n");
    } else {
        for(const char *const *dir = data_dirs; *dir; dir++) {
            printf(" - '%s'\n", *dir);
        }
    }

    printf("\nXDG_CONFIG_DIRS:\n");
    const char *const *config_dirs = xdg_dirs_config_dirs();
    if(!*data_dirs) {
        printf("<empty>\n");
    } else {
        for(const char *const *dir = config_dirs; *dir; dir++) {
            printf(" - '%s'\n", *dir);
        }
    }

    const char *FILE_NAME = "test_file.txt";
    printf("\nSearching for '%s' in config dirs: '%s'\n", FILE_NAME,
           xdg_dirs_search_config_dirs(FILE_NAME));
    printf("Searching for '%s' in data dirs: '%s'\n", FILE_NAME,
           xdg_dirs_search_data_dirs(FILE_NAME));

    printf("\nResetting values...\n");
    xdg_dirs_reset();

    putenv("XDG_DATA_HOME=/home/jon/Download");
    printf("New XDG_DATA_HOME = '%s'\n", xdg_dirs_data_home());

}
